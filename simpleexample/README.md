# Simpleexample

Dette prosjektet er et eksempel på en nokså minimalistisk trelagsapplikasjon, altså med et domenelag, brukergrensesnitt (UI) og persistens (lagring). Prosjektet inneholder tester for alle lagene, med rimelig god dekningsgrad. Prosjektet er konfigurert med **maven** som byggesystem.

## Organisering av koden

Prosjektet er organisert med 2 * 2 = 4 kildekodemapper, kode og ressurser for henholdsvis applikasjonen selv og testene:

- **src/main/java** for koden til applikasjonen
- **src/main/resources** for tilhørende ressurser, f.eks. data-filer og FXML-filer, som brukes av applikasjonen.
- **src/test/java** for testkoden
- **src/test/resources** for tilhørende ressurser, f.eks. data-filer og FXML-filer, som brukes av testene.

Dette er en vanlig organisering for prosjekter som bygges med maven (og gradle).

## Domenelaget

Domenelaget inneholder alle klasser og logikk knyttet til dataene som applikasjonen handler om og håndterer. Dette laget skal være helt uavhengig av brukergrensesnittet eller lagingsteknikken som brukes.

Vår app handler om samlinger av såkalte geo-lokasjoner, altså steder identifisert med lengde- og breddegrader. Domenelaget inneholder klasser for å representere og håndtere slike, og disse finnes i **[simpleex.core](src/main/java/simpleex/core/README.md)**-pakken.

## Brukergrensesnittlaget

Brukergrensesnittlaget inneholder alle klasser og logikk knyttet til visning og handlinger på dataene i domenelaget. Brukergrensesnittet til vår app viser frem en liste av geo-lokasjoner, den som velges vises på et kart. En flytte og legge til geo-lokasjoner. Samlingen med geo-lokasjoner kan lagres og evt. leses inn igjen.

Brukergrensesnittet er laget med JavaFX og FXML og finnes i **[simpleex.ui](src/main/java/simpleex/ui/README.md)**-pakken (JavaFX-koden i **src/main/java** og FXML-filen i **src/main/resources**)

## Persistenslaget

Persistenslaget inneholder alle klasser og logikk for lagring (skriving og lesing) av dataene i domenelaget. Vårt persistenslag implementerer fillagring med JSON-syntaks.

Persistenslaget finnes i **[simpleex.json](src/main/java/simpleex/json/README.md)**-pakken.

## Bygging med maven

For litt mer komplekse prosjekter, er det lurt å bruke et såkalt byggeverktøy, f.eks. maven eller gradle, for å automatisere oppgaver som kjøring av tester, sjekk av ulike typer kvalitet osv.
Vårt prosjekt er konfigurert til å bruke [maven](https://maven.apache.org), og følgelig har prosjektet en pom.xml-fil for konfigurajon.

En pom.xml-fil inneholder ulike typer informasjon om prosjektet:

- *identifikasjon* i form av **groupId**-, **artifactId**- og **version**-elementer
- *avhengigheter* i form av **dependency*-elementer
- *byggetillegg (plugins)* i form av **plugin**-elementer, som igjen konfigureres med **configuration**-elementer

Vårt bygg har tillegg for:

- oppsett av java (**maven-compiler-plugin**)
- testing (**maven-surefire-plugin**)
- kjøring av javafx (**javafx-maven-plugin**)
- sjekking av kodekvalitet med [checkstyle](https://checkstyle.sourceforge.io) (**maven-checkstyle-plugin**) og [spotbugs](https://spotbugs.github.io) (**spotbugs-maven-plugin**)
- testdekningsgrad med [jacoco](https://github.com/jacoco/jacoco) (**jacoco-maven-plugin**)

De fleste avhengighetene hentes fra de vanlige sentrale repo-ene, med unntak av FxMapControl som er lagt ved som jar-fil i **libs**-mappa.
Det siste krever manuell bruk av maven install, se **tasks**-delen av [.gitpod.yml](../.gitpod.yml)
