module simpleexample {
    requires transitive com.fasterxml.jackson.core;
    requires transitive com.fasterxml.jackson.databind;

    requires javafx.fxml;
    requires transitive javafx.graphics;
    requires javafx.controls;

    requires fx.map.control;

    exports simpleex.core;
    exports simpleex.json;
    exports simpleex.ui;

    opens simpleex.ui to javafx.fxml;
}
